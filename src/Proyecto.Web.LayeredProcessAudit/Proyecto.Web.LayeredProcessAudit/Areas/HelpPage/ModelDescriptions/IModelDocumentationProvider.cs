using System;
using System.Reflection;

namespace Proyecto.Web.LayeredProcessAudit.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}