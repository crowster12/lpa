
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/22/2015 10:17:59
-- Generated from EDMX file: C:\Users\Crowster\Documents\Visual Studio 2013\Projects\Sesiones\Sesiones\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Participacion];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Calendarios]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Calendarios];
GO
IF OBJECT_ID(N'[dbo].[Humano]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Humano];
GO
IF OBJECT_ID(N'[dbo].[LastTarget]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LastTarget];
GO
IF OBJECT_ID(N'[dbo].[nombresTarjetasSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[nombresTarjetasSet];
GO
IF OBJECT_ID(N'[dbo].[Participacion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Participacion];
GO
IF OBJECT_ID(N'[dbo].[PARTICIPACIONB]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PARTICIPACIONB];
GO
IF OBJECT_ID(N'[dbo].[Table]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Table];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Humanoes'
CREATE TABLE [dbo].[Humanoes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ojos] nvarchar(50)  NULL,
    [nariz] nvarchar(50)  NULL,
    [boca] nvarchar(50)  NULL
);
GO

-- Creating table 'nombresTarjetasSets'
CREATE TABLE [dbo].[nombresTarjetasSets] (
    [idTarjeta] int IDENTITY(1,1) NOT NULL,
    [nombreTarjetaSet] varchar(50)  NULL,
    [area] varchar(50)  NULL,
    [nave] varchar(50)  NULL,
    [otro] varchar(50)  NULL,
    [incremento] varchar(50)  NULL
);
GO

-- Creating table 'Participacions'
CREATE TABLE [dbo].[Participacions] (
    [nocontrol] nvarchar(50)  NULL,
    [NOMBRE] varchar(50)  NULL,
    [APPAT] varchar(50)  NULL,
    [APMAT] varchar(50)  NULL,
    [CAPA] varchar(50)  NULL,
    [AREA] varchar(50)  NULL,
    [PUESTO_GENERAL] varchar(50)  NULL,
    [GRUPO] varchar(50)  NULL,
    [SUPERVISOR] varchar(50)  NULL,
    [LIDER_AREA] varchar(50)  NULL,
    [FRECUENCIA] varchar(50)  NULL,
    [PARTICIPACION1] varchar(50)  NULL,
    [ID_USUARIO] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'PARTICIPACIONBs'
CREATE TABLE [dbo].[PARTICIPACIONBs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nocontrol] varchar(50)  NULL,
    [NOMBRE] varchar(50)  NULL,
    [APPAT] varchar(50)  NULL,
    [APMAT] varchar(50)  NULL,
    [CAPA] varchar(50)  NULL,
    [AREA] varchar(50)  NULL,
    [PUESTOGENERAL] varchar(50)  NULL,
    [GRUPO] varchar(50)  NULL,
    [SUPERVISOR] varchar(50)  NULL,
    [LIDERAREA] varchar(50)  NULL,
    [FRECUENCIA] varchar(50)  NULL,
    [PARTICIPACION] varchar(50)  NULL
);
GO

-- Creating table 'Tables'
CREATE TABLE [dbo].[Tables] (
    [IDUsuario] int IDENTITY(1,1) NOT NULL,
    [FullName] varchar(100)  NOT NULL,
    [UserName] varchar(50)  NOT NULL,
    [Password] varchar(50)  NOT NULL
);
GO

-- Creating table 'LastTargets'
CREATE TABLE [dbo].[LastTargets] (
    [IdTarjeta] int  NOT NULL,
    [ultimaTarjeta] varchar(50)  NULL,
    [area] varchar(50)  NULL,
    [noTarjeta] varchar(50)  NULL
);
GO

-- Creating table 'Calendarios'
CREATE TABLE [dbo].[Calendarios] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL,
    [lpa5] varchar(50)  NULL,
    [lpa6] varchar(50)  NULL,
    [lpa7] varchar(50)  NULL,
    [lpa8] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioOtrosCoaches'
CREATE TABLE [dbo].[CalendarioOtrosCoaches] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioFormings'
CREATE TABLE [dbo].[CalendarioFormings] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioEmpleados'
CREATE TABLE [dbo].[CalendarioEmpleados] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioSupervisors'
CREATE TABLE [dbo].[CalendarioSupervisors] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL,
    [lpa5] varchar(50)  NULL,
    [lpa6] varchar(50)  NULL,
    [lpa7] varchar(50)  NULL,
    [lpa8] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioAlmacens'
CREATE TABLE [dbo].[CalendarioAlmacens] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioRovings'
CREATE TABLE [dbo].[CalendarioRovings] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'NLTs'
CREATE TABLE [dbo].[NLTs] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioHBSs'
CREATE TABLE [dbo].[CalendarioHBSs] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- Creating table 'CalendarioTecnicos'
CREATE TABLE [dbo].[CalendarioTecnicos] (
    [Id] int  NOT NULL,
    [nombre] varchar(50)  NULL,
    [lpa1] varchar(50)  NULL,
    [lpa2] varchar(50)  NULL,
    [lpa3] varchar(50)  NULL,
    [lpa4] varchar(50)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Humanoes'
ALTER TABLE [dbo].[Humanoes]
ADD CONSTRAINT [PK_Humanoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [idTarjeta] in table 'nombresTarjetasSets'
ALTER TABLE [dbo].[nombresTarjetasSets]
ADD CONSTRAINT [PK_nombresTarjetasSets]
    PRIMARY KEY CLUSTERED ([idTarjeta] ASC);
GO

-- Creating primary key on [ID_USUARIO] in table 'Participacions'
ALTER TABLE [dbo].[Participacions]
ADD CONSTRAINT [PK_Participacions]
    PRIMARY KEY CLUSTERED ([ID_USUARIO] ASC);
GO

-- Creating primary key on [Id] in table 'PARTICIPACIONBs'
ALTER TABLE [dbo].[PARTICIPACIONBs]
ADD CONSTRAINT [PK_PARTICIPACIONBs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [IDUsuario] in table 'Tables'
ALTER TABLE [dbo].[Tables]
ADD CONSTRAINT [PK_Tables]
    PRIMARY KEY CLUSTERED ([IDUsuario] ASC);
GO

-- Creating primary key on [IdTarjeta] in table 'LastTargets'
ALTER TABLE [dbo].[LastTargets]
ADD CONSTRAINT [PK_LastTargets]
    PRIMARY KEY CLUSTERED ([IdTarjeta] ASC);
GO

-- Creating primary key on [Id] in table 'Calendarios'
ALTER TABLE [dbo].[Calendarios]
ADD CONSTRAINT [PK_Calendarios]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioOtrosCoaches'
ALTER TABLE [dbo].[CalendarioOtrosCoaches]
ADD CONSTRAINT [PK_CalendarioOtrosCoaches]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioFormings'
ALTER TABLE [dbo].[CalendarioFormings]
ADD CONSTRAINT [PK_CalendarioFormings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioEmpleados'
ALTER TABLE [dbo].[CalendarioEmpleados]
ADD CONSTRAINT [PK_CalendarioEmpleados]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioSupervisors'
ALTER TABLE [dbo].[CalendarioSupervisors]
ADD CONSTRAINT [PK_CalendarioSupervisors]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioAlmacens'
ALTER TABLE [dbo].[CalendarioAlmacens]
ADD CONSTRAINT [PK_CalendarioAlmacens]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioRovings'
ALTER TABLE [dbo].[CalendarioRovings]
ADD CONSTRAINT [PK_CalendarioRovings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NLTs'
ALTER TABLE [dbo].[NLTs]
ADD CONSTRAINT [PK_NLTs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioHBSs'
ALTER TABLE [dbo].[CalendarioHBSs]
ADD CONSTRAINT [PK_CalendarioHBSs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CalendarioTecnicos'
ALTER TABLE [dbo].[CalendarioTecnicos]
ADD CONSTRAINT [PK_CalendarioTecnicos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------